var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    firstName: String,
    lastName: String,
    email : String,
    password: String,
    hasAdminRights: { type: Boolean, default:false}
});

var User = mongoose.model('User', userSchema);

module.exports = User;