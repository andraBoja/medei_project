var express = require('express');
var router = express.Router();
var User = require('../models/User');
var bcrypt = require('bcrypt');

/**
 * Checks user credentials
 * @route /login
 * @method POST
 */
router.post('/login', function (req, res) {
    User.find({"email": req.body.email}, function (err, foundUser) {
        if (err) {
            console.log(err);
        }
        else {
            if (foundUser.length == 0) {
                console.log("user not found");
                console.log(req.body);
                res.send(404);
            }
            else {
                if (bcrypt.compareSync(req.body.password, foundUser[0].password)) {
                    res.send({token: foundUser[0]._id}, 200);
                }
                else {
                    res.send(403)
                }

            }
        }
    })
});

module.exports = router;
