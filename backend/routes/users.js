var express = require('express');
var router = express.Router();
var User = require('../models/User');
var bcrypt = require('bcrypt');


function removeSensitiveData(user) {
    var copyUser = JSON.parse(JSON.stringify(user));
    delete copyUser.password;
    delete copyUser._id;
    return copyUser;
}

/**
 * Read all users
 * @route /users
 * @method GET
 * @restricted admin
 */
router.get('/', function (req, res) {
    if (req.query.token) {
        User.find({"_id": req.query.token}, function (err, tokenUsers) {
            if (err) {
                console.log(err);
                res.send(500);
            } else {
                if (tokenUsers.length == 1) {
                    var tokenUser = tokenUsers[0];
                    if (tokenUser.hasAdminRights) {
                        User.find({}, function (err, users) {
                            if (err) {
                                console.log(err);
                                res.send(500);
                            } else {
                                var foundUsers = [];
                                for (var i = 0; i < users.length; i++) {
                                    foundUsers.push(removeSensitiveData(users[i]));
                                }
                                res.send(foundUsers);
                            }
                        });
                    } else {
                        res.send(403);
                    }
                } else {
                    res.send(403);
                }
            }
        });

    } else {
        res.send(403); //Forbidden
    }
});

/**
 * Create an user
 * @route /users
 * @method POST
 */
router.post('/', function (req, res) {
    var user = new User();
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.email = req.body.email;
    user.hasAdminRights = req.body.hasAdminRights;
    var salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(req.body.password, salt);
    user.save(function (err) {
        if (err) {
            console.log(err);
            res.send(400);
        }
        else {
            res.send(200);
        }
    })
});

/**
 * Updates an user based on the email
 * @route /users/:email
 * @method PUT
 * @restricted admin|specific user
 */
router.put('/:email', function (req, res) {
    if (req.query.token) {
        var searchEmail = req.param("email");
        User.find({"_id": req.query.token}, function (err, tokenUsers) {
            if (err) {
                console.log(err);
                res.send(500);
            } else {
                if (tokenUsers.length == 1) {
                    var tokenUser = tokenUsers[0];
                    if (tokenUser.hasAdminRights || tokenUser.email == searchEmail) {
                        User.find({"email": searchEmail}, function (err, foundUsers) {
                            if (err) {
                                console.error(err);
                                res.send(500);
                            }
                            else {
                                if (foundUsers.length == 0) {
                                    console.log("user not found");
                                    res.send(404);
                                } else if (foundUsers.length > 1) {
                                    console.log("trying to update and found more then one user with email: " + searchEmail);
                                    res.send(500);
                                }
                                else {
                                    //we found the unique user
                                    var foundUser = foundUsers[0];

                                    if (req.body.firstName) {
                                        foundUser.firstName = req.body.firstName;
                                    }

                                    if (req.body.lastName) {
                                        foundUser.lastName = req.body.lastName;
                                    }

                                    if (req.body.password) {
                                        var salt = bcrypt.genSaltSync(10);
                                        foundUser.password = bcrypt.hashSync(req.body.password, salt);
                                    }

                                    if (req.body.hasAdminRights !== undefined) {
                                        foundUser.hasAdminRights = req.body.hasAdminRights;
                                    }
                                    foundUser.save(function (err) {
                                        if (err) {
                                            console.log(err);
                                            res.send(400);
                                        }
                                        else {
                                            res.send(200);
                                        }
                                    })
                                }
                            }
                        })
                    } else {
                        res.send(403);
                    }
                } else {
                    res.send(403);
                }
            }
        });
    } else {
        res.send(403);
    }
});

/**
 * Reads an user based on the email
 * @route /users/:email
 * @method GET
 */
router.get('/:email', function (req, res) {
    if (req.query.token) {
        var searchEmail = req.param("email");
        User.find({"_id": req.query.token}, function (err, tokenUsers) {
            if (err) {
                console.log(err);
                res.send(500);
            } else {
                if (tokenUsers.length == 1) {
                    var tokenUser = tokenUsers[0];
                    if (tokenUser.hasAdminRights || tokenUser.email == searchEmail) {
                        User.find({"email": searchEmail}, function (err, foundUsers) {
                            if (err) {
                                console.error(err);
                                res.send(500);
                            } else {
                                User.find({"email": searchEmail}, function (err, foundUsers) {
                                    if (err) {
                                        console.log(err);
                                        res.send(500);
                                    }
                                    else if (foundUsers.length > 1) {
                                        console.log("trying to get and found more then one user with email: " + searchEmail);
                                        res.send(500);
                                    }
                                    else {
                                        if (foundUsers.length == 0) {
                                            console.log("user not found");
                                            res.send(404);
                                        }
                                        else {
                                            res.send(removeSensitiveData(foundUsers[0]));
                                        }
                                    }
                                })
                            }
                        });
                    } else {
                        res.send(403);
                    }
                } else {
                    res.send(403);
                }
            }
        });
    } else {
        res.send(403);
    }
});

module.exports = router;
