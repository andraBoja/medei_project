angular.module('medeiApp')
    .controller('UserCtrl', ['$scope', '$http', '$state',
        function ($scope, $http, $state) {
            $scope.hasError = false;
            $scope.userAdded = null;
            $scope.email = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;

            function fetchUser() {
                var email = localStorage.getItem('email');
                var token = localStorage.getItem('token');
                $http.get('http://localhost:3000/users/' + email + "?token=" + token).then(function (res) {
                    $scope.user = res.data;
                    $scope.editedUser = {
                        firstName: res.data.firstName,
                        lastName: res.data.lastName,
                        password: ""
                    }
                })
            }

            $scope.login = function (user) {
                var data = {
                    email: user.email,
                    password: user.password
                };
                $http.post('http://localhost:3000/login', data).then(function (response) {
                    localStorage.setItem('email', user.email);
                    localStorage.setItem('token', response.data.token);


                    $state.go('profile', {user: $scope.user});
                }, function (err) {
                    $scope.hasError = true;
                });
            };

            if ($state.current.name == 'profile') {
                fetchUser();
            }

            if ($state.current.name == 'users') {
                var email = localStorage.getItem('email');
                var token = localStorage.getItem('token');
                if (!token) {
                    $scope.view_error = "You must log in first";
                } else {
                    $http.get('http://localhost:3000/users?token=' + token).then(function (response) {
                        $scope.view_error = "";
                        $scope.users = response.data;
                    }, function (err) {
                        $scope.view_error = "You don't have admin rights";
                    });
                }
            }




            $scope.editUser = function(editedUser){
                var email = localStorage.getItem('email');
                var token = localStorage.getItem('token');
                if (!token) {
                    $scope.view_error = "You must log in first";
                } else {
                    $http.put('http://localhost:3000/users/' + email +'?token=' + token, $scope.editedUser).then(function (response) {
                        $scope.view_error = "";
                        $scope.logOut();
                    }, function (err) {
                        $scope.view_error = "You don't have admin rights to edit other peoples data";
                        $scope.logOut();
                    });
                }
            };
            $scope.logOut = function () {
                localStorage.removeItem('email');
                localStorage.removeItem('token');
                $state.go('login', {user: null});
            };

            $scope.reset = function () {
                $scope.user = {};
            };

            $scope.addUser = function (user) {
                var data = {
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    password: user.password,
                    hasAdminRights: user.hasAdminRights
                };
                if ($scope.newUserForm.$valid) {
                    $http.post('http://localhost:3000/users', data).then(function (response) {
                        console.log("User added " + response);
                        $scope.userAdded = true;
                        alert("user created");
                        $state.go('login')
                    }, function (err) {
                        $scope.userAdded = false;
                        alert("user NOT created");
                        $state.go('addUser')
                    });
                }

                return false;
            }
        }])

    .directive('compareTo', function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    });
//.directive('modal', function () {
//    return {
//        templateUrl: "view/users.html",


