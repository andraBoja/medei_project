angular.module('medeiApp', ['ui.router', 'ngMessages']).config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");
    //
    // Now set up the states
    $stateProvider
        .state('index', {
            url: "/",
            controller: "UserCtrl"
        })
        .state('users', {
            url: "/users",
            templateUrl: "view/users.html",
            controller: "UserCtrl"
        })
        .state('login', {
            url: "/login",
            templateUrl: "view/login.html",
            controller: "UserCtrl"
        })
        .state('addUser', {
            url: "/addUser",
            templateUrl: "view/addUser.html",
            controller: "UserCtrl"
        })
        .state('profile', {
            url: "/profile",
            templateUrl: "view/profile.html",
            controller: "UserCtrl"
        });
});